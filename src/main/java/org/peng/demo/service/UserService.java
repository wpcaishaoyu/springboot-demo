package org.peng.demo.service;

import org.peng.demo.dao.UserMapper;
import org.peng.demo.model.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by caicai on 2016/6/27.
 */
@Service
public class UserService {
    @Resource
    private UserMapper userDAO;
    public User getById(Integer id){
        return userDAO.selectById(Long.valueOf(id));
    }
}
