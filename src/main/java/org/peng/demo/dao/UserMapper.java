package org.peng.demo.dao;

import com.baomidou.mybatisplus.mapper.AutoMapper;
import org.peng.demo.model.User;

public interface UserMapper extends AutoMapper<User>{

}