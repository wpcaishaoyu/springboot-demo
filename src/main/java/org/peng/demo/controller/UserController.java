package org.peng.demo.controller;

import org.peng.demo.model.User;
import org.peng.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by caicai on 2016/6/15.
 */
@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @Value("${application.message:Hello World}")
    String message="Hello World";
    @RequestMapping("/")
    @ResponseBody
    String hello(){
        return "Hello World";
    }
    @RequestMapping("/user")
    String userInfo(Map<String,Object> model){
        model.put("time",new SimpleDateFormat("yyyy年MM月dd日").format(new Date()));
        model.put("message", message);
        return "user_add";
    }
    @RequestMapping("/test")
    public String getUser(Map<String,Object> model,Integer id){
        User user=userService.getById(id);
        model.put("name",user.getName());
        model.put("age",user.getAge());
        return "test";
    }
}
