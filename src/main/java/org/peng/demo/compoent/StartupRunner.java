package org.peng.demo.compoent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Created by caicai on 2016/7/25.
 */
@Order(1)
@Component
public class StartupRunner implements CommandLineRunner{
    private static final Logger logger = LoggerFactory
            .getLogger(CommandLineRunner.class);
    @Override
    public void run(String... strings) throws Exception {
       logger.info("spring boot start up");
    }
}
