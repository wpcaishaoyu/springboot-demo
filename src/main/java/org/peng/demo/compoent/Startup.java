package org.peng.demo.compoent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Created by caicai on 2016/7/25.
 */
/**
 * 对于那种只需要在应用程序启动时执行一次的任务，非常适合利用Command line runners来完成。
 * Spring Boot应用程序在启动后，会遍历CommandLineRunner接口的实例并运行它们的run方法。
 * 也可以利用@Order注解（或者实现Order接口）来规定所有CommandLineRunner实例的运行顺序。
 */
@Order(2)
@Component
public class Startup implements CommandLineRunner{//
    private static final Logger logger = LoggerFactory
            .getLogger(CommandLineRunner.class);
    @Override
    public void run(String... strings) throws Exception {
        logger.info("spring boot 2 start up");
    }
}
